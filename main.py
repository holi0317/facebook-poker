"""
Respond to Facebook pokes from others.

Requirement:
    - Firefox is instaled
    - Facebook language is English(US)
    - Have a pass entry for facebook credentials
"""
import os
import sys
import logging
import subprocess
from time import sleep
from typing import Dict, List

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

POKE_URL = 'https://www.facebook.com/pokes/'
PASS_ENTRY = 'social/facebook'

logger = logging.getLogger('poker')

def tick(driver):
    try:
        links = driver.find_elements_by_tag_name('a')
        pokes = [link for link in links if link.get_property('innerText') == 'Poke Back']

        for poke in pokes:
            poke.click()
    except StaleElementReferenceException:
        logger.debug('Some element is stale')
        return

    if len(pokes) > 0:
        logger.info(f'Poked {len(pokes)} people')
    else:
        logger.debug('No people available for poking')


def parse_entry(entry: List[str]) -> Dict[str, str]:
    """
    Parse pass entry data.
    This function does not extract password from given entry.

    Args:
        entry - Password entry that already splitted by \n
    """
    items = entry[1:]
    result = {}

    for item in entry[1:]:
        pair = item.split(': ')
        if len(pair) == 2:
            result[pair[0]] = pair[1]

    return result


def login(driver):
    if os.getenv('PASS_ENTRY_CONTENT') != None:
        res = os.getenv('PASS_ENTRY_CONTENT')
        entry = res.split('\n')
    else:
        res = subprocess.run(['pass', PASS_ENTRY], check=True, capture_output=True)
        entry = res.stdout.decode().split('\n')

    login = parse_entry(entry)['login']
    password = entry[0]

    driver.find_element_by_id('email').send_keys(login)
    driver.find_element_by_id('pass').send_keys(password)
    driver.find_element_by_id('loginbutton').click()

    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_to_be(POKE_URL))


def main():
    profile = webdriver.FirefoxProfile()
    # Disable web notification
    profile.set_preference('permissions.default.desktop-notification', 2)

    options = Options()
    options.add_argument("--headless")

    driver = webdriver.Firefox(firefox_profile=profile, firefox_options=options)
    driver.get(POKE_URL)

    if driver.current_url != POKE_URL:
        logger.info('Trying to login to Facebook using pass data')
        login(driver)

    logger.info('Starting to watch for poking')
    try:
        # Restart browser every hour
        for _ in range(60):
            # Reload the page every minute
            for _ in range(30):
                tick(driver)
                sleep(2)
            logger.debug('Refreshing page')
            driver.refresh()

        logger.debug('Terminating browser')
    finally:
        driver.close()


if __name__ == '__main__':
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '[%(asctime)s] [%(name)s / %(levelname)s] %(message)s', '%H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    while True:
        main()
