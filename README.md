# poker
Respond to pokes from Facebook automatically

## Requirements
 - Python
 - pass is installed and facebook credential is in `social/facebook`
 - Facebook use English(US) as language

## Installation
```bash
# For installing dependency
pip install -r requirements.txt

# For starting the script
python main.py
```

## Behavior
 - After starting the script, it will launch the browser and login
 - It will look for available `poke back` every 2 seconds
 - After a minute, the script will refresh the page to make sure there is no missing poke

## Got `Poke element is stale` warning
That means the button has been clicked when the script tries to click it.

This warning should be fine as the script will recover from this exception.

## Use without pass
Set environment variable `PASS_ENTRY_CONTENT` to the content of pass entry for skipping
issue of pass command.
